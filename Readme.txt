Project 1.1
Andrew Heyman


1) Run the deploy.sh in a bash terminal
- This creates 1 event manager, 3 publishers, 3 subscribers

2) Record the event manager IP address
docker inspect eventmanager | grep -i ipaddress 

3) Attach to a docker container and run the appropriate version:

[Event Manager, where port is an optional argument, defaults to 8000]
docker attach eventmanager
cd /csci652-aheyman-proj-1_1 && java -cp build edu/rit/CSCI652/impl/EventManager (port)


[Publisher (1,2,3), arguments are required]
docker attach publisher(1-3)
cd /csci652-aheyman-proj-1_1 && java -cp build edu/rit/CSCI652/impl/PubAgent (eventmanager IP address) (eventmanager port)


[Subscriber (1,2,3), arguments are required]
docker attach subscriber(1-3)
cd /csci652-aheyman-proj-1_1 && java -cp build edu/rit/CSCI652/impl/SubAgent (eventmanager IP address) (event manager port) (port to listen for events)


