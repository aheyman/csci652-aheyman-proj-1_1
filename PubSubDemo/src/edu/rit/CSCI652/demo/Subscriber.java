/**
 * Author:  Andrew Heyman
 * Course:  CSCI-652
 * Project: Project 1.1
 */

package edu.rit.CSCI652.demo;

import java.util.List;

public interface Subscriber {
	/*
	 * subscribe to a topic
	 */
	void subscribe(Topic topic);
	
	/*
	 * subscribe to a topic with matching keywords
	 */
	void subscribe(String keyword);
	
	/*
	 * unsubscribe from a topic 
	 */
	void unsubscribe(Topic topic);
	
	/*
	 * unsubscribe to all subscribed topics
	 */
	void unsubscribe();
	
	/*
	 * show the list of topics current subscribed to 
	 */
	List listSubscribedTopics();
	
}
