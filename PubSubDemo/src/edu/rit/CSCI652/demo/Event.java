/*
 * Author:  Andrew Heyman
 * Course:  CSCI-652
 * Project: Project 1.1
 */

package edu.rit.CSCI652.demo;


import java.io.Serializable;

public class Event implements Serializable{
	private int id;
	private Topic topic;
	private String title;
	private String content;

	public Event(final Topic tp, final String title, final String content){

		final int PRIME_INT = 31;

		this.topic = tp;
		this.title = title;
		this.content = content;

		// cache the Hashcode so it's only calculated on object creation
		this.id = ((tp.hashCode() + title.hashCode() + content.hashCode()) * PRIME_INT) % Integer.MAX_VALUE;
	}

	public int getId() {
		return this.id;
	}

	public Topic getTopic() {
		return topic;
	}

	public String getTitle() {
		return title;
	}

	public String getContent() {
		return content;
	}

	@Override
	public boolean equals(Object o) {

		if ( (o instanceof Event) ){
			Event that = (Event)o;
			return 	that.topic.equals(this.topic) &&
					that.title.equals(this.title) &&
					that.content.equals(this.content);
		} else{
			return false;
		}

	}

	@Override
	public int hashCode() {
		return this.id;
	}

    @Override
    public String toString() {
        return "Topic: " + topic.getName() + System.getProperty("line.separator")
                + "Event: "  + getTitle() + System.getProperty("line.separator")
                + "Content: " + getContent();
    }
}
