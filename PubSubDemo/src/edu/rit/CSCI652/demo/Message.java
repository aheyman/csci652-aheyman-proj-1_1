/*
 * Author:  Andrew Heyman
 * Course:  CSCI-652
 * Project: Project 1.1
 */

package edu.rit.CSCI652.demo;

import java.io.Serializable;

/**
 * Base class for message passing
 */
public abstract class Message implements Serializable{

    private Operation operation;

    public Message(Operation op){
        this.operation = op;
    }

    public Operation getOperation(){
        return this.operation;
    }
}
