/**
 * Author:  Andrew Heyman
 * Course:  CSCI-652
 * Project: Project 1.1
 */

package edu.rit.CSCI652.demo;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;

public class Topic implements Serializable{
	private int id;
	private List<String> keywords;
	private String name;

	public Topic(List<String> keywords, String name){

        if (name == null){
            throw new IllegalArgumentException("Topic name cannot be null");
        } else if (keywords == null || keywords.isEmpty()){
            throw new IllegalArgumentException("Keywords cannot be empty");
        }

        // Maintain order for ease of comparison
        Collections.sort(keywords);

	    this.keywords = keywords;
	    this.name = name;

	    long keywordHash = keywords.stream().mapToLong(String::hashCode).sum();

	    this.id = (int)keywordHash + name.hashCode();
    }

    public int getId() {
        return id;
    }

    public List<String> getKeywords() {
        return keywords;
    }

    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object o) {

	    if (o instanceof Topic){
	        Topic that = (Topic)o;
	        return that.name.equals(this.name) && that.keywords.equals(this.keywords);
        } else{
	        return false;
        }

    }

    @Override
    public int hashCode() {
        return this.id;
    }

    @Override
    public String toString() {
        return "Topic Name: " + name;
    }
}
