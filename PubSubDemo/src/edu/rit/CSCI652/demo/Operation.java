/*
 * Author:  Andrew Heyman
 * Course:  CSCI-652
 * Project: Project 1.1
 */

package edu.rit.CSCI652.demo;

public enum Operation{
    CACHE_CHECK, PUBLISH, ADVERTISE, SUBSCRIBE, UNSUBSCRIBE, LIST_SUBSCRIBED_TOPICS, LIST_ALL_TOPICS
}