/*
 * Author:  Andrew Heyman
 * Course:  CSCI-652
 * Project: Project 1.1
 */

package edu.rit.CSCI652.demo;

public class PublisherMessage extends Message{

    private Event event;
    private Topic topic;


    public PublisherMessage(final Operation op){
        super(op);
        if (op == Operation.LIST_ALL_TOPICS){
        } else{
            throw new IllegalArgumentException("Cannot perform " + op + " with Events");
        }
    }

    public PublisherMessage(final Operation op, final Event event){
        super(op);
        if (op == Operation.PUBLISH){
            this.event = event;
        } else{
            throw new IllegalArgumentException("Cannot perform " + op + " with Events");
        }
    }

    public PublisherMessage(final Operation op, final Topic topic){
        super(op);
        if (op == Operation.ADVERTISE){
            this.topic = topic;
        } else{
            throw new IllegalArgumentException("Cannot perform " + op + " with Topics");
        }
    }

    public Event getEvent() {
        return event;
    }

    public Topic getTopic() {
        return topic;
    }
}
