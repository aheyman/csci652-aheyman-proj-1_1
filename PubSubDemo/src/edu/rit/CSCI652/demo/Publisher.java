/*
 * Author:  Andrew Heyman
 * Course:  CSCI-652
 * Project: Project 1.1
 */

package edu.rit.CSCI652.demo;

public interface Publisher {
	/*
	 * publish an event of a specific topic with title and content
	 */
	void publish(Event event);
	
	/*
	 * advertise new topic
	 */
	void advertise(Topic newTopic);
}
