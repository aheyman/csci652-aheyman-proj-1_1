/**
 * Author:  Andrew Heyman
 * Course:  CSCI-652
 * Project: Project 1.1
 */

package edu.rit.CSCI652.demo;

public class SubscriberMessage extends Message {

    private Topic topic;
    private String keyword;
    private int listeningPort;

    public SubscriberMessage(Operation op, int listeningPort){
        super(op);
        this.listeningPort = listeningPort;
    }

    public SubscriberMessage(Operation op, Topic top, int listeningPort) {
        super(op);
        this.topic = top;
        this.listeningPort = listeningPort;

    }

    public SubscriberMessage(Operation op, String keyword ){
        super(op);
        this.keyword = keyword;

    }

    public Topic getTopic() {
        return this.topic;
    }

    public String getKeyword(){
        return this.keyword;
    }

    public int getListeningPort() {
        return listeningPort;
    }
}



