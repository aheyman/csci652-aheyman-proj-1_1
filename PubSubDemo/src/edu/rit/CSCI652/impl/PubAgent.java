/**
 * Author:  Andrew Heyman
 * Course:  CSCI-652
 * Project: Project 1.1
 */

package edu.rit.CSCI652.impl;

import edu.rit.CSCI652.demo.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;


public class PubAgent extends Agent implements Publisher{

    /**
     * Creates a publisher node
     * @param eventManagerIP ip address of event manager
     * @param port port of event mananger
     */
    public PubAgent(String eventManagerIP, int port){
        super(eventManagerIP, port);
    }

    /**
     * Sends the event to the Event Manager
     * @param event event to publish
     */
    @Override
    public void publish(Event event) {

        submitMessage(new PublisherMessage(Operation.PUBLISH, event));
    }

    /**
     * Sends the topic to the Event Manager
     * @param newTopic topic to publish
     */
    @Override
    public void advertise(Topic newTopic) {
        submitMessage(new PublisherMessage(Operation.ADVERTISE, newTopic));
    }

    public List viewTopics(){
        Object obj = submitAndReceive((new PublisherMessage(Operation.LIST_ALL_TOPICS)));

        if (obj instanceof List){
            return (List)obj;
        } else{
            return null;
        }
    }

    public static void main(String[] args){

        final String END = "quit";
        int eventManagerPort = -1;
        String eventManagerIP = "";

        if (args != null && args.length == 2){
            eventManagerIP = args[0];
            eventManagerPort = Integer.parseInt(args[1]);

        } else {
            System.out.println("Must have either 2 arguments");
            System.out.println("-arg 1: event manager IP, -arg 2: event manager port");
            System.exit(1);
        }
        System.out.println("Connection to event manager on port: " + eventManagerPort);
        PubAgent agent = new PubAgent(eventManagerIP, eventManagerPort);

        //=============
        // Simple UI
        //=============
        System.out.println("\'1\' to create a new topic");
        System.out.println("\'2\' to create a new event");
        System.out.println("\'Quit\' to end the publisher");
        Scanner sc = new Scanner(System.in);
        String input = sc.nextLine();
        while (!input.toLowerCase().equals(END)){

            if (Integer.parseInt(input) == 1){
                System.out.println("Please provide topic title & Keywords (Separated by ;)");
                System.out.println("Title: ");
                String title = sc.nextLine();
                System.out.println("Keywords (separated by ;): ");
                String[] keywords = sc.nextLine().split(";");

                agent.advertise(new Topic(Arrays.asList(keywords),title));

            }

            if (Integer.parseInt(input) == 2){

                List result = agent.viewTopics();
                Object[] array = result.toArray();

                System.out.println("Please select a topic");

                for (int idx = 0; idx < array.length; idx++){
                    System.out.println("" + idx + ". " + array[idx]);
                }


                input = sc.nextLine();

                Topic tp = (Topic)array[Integer.parseInt(input)];

                System.out.println("Please provide title and content");
                System.out.println("Title: ");
                String title = sc.nextLine();
                System.out.println("Content: ");
                String content = sc.nextLine();

                agent.publish(new Event(tp, title, content));

            }

            System.out.print(">");
            input = sc.nextLine();

        }

        agent.threadPool.shutdown();
        System.exit(0);
    }


}
