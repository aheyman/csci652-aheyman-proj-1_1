/**
 * Author:  Andrew Heyman
 * Course:  CSCI-652
 * Project: Project 1.1
 */

package edu.rit.CSCI652.impl;

import edu.rit.CSCI652.demo.Message;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.concurrent.*;

public abstract class Agent {
    final ExecutorService threadPool = Executors.newFixedThreadPool(5); // Listening and Sending
    private String eventManager;
    private int eventManagerPort;

    Agent(String eventManagerIP, int eventManagerPort){
        eventManager = eventManagerIP;
        this.eventManagerPort = eventManagerPort;
    }

    void submitMessage(Message message){

        Socket socket = null;

        try {
            socket = new Socket(eventManager, eventManagerPort);
            Runnable runnable = new ConnectionWrite(socket, message);
            threadPool.execute(runnable);
        } catch (IOException e) {
            System.out.println("Issue with the request");
            e.printStackTrace();
        }
    }

    Object submitAndReceive(Message message) {

        Socket socket = null;
        Future<Object> result = null;

        try {
            socket = new Socket(eventManager, eventManagerPort);
            Callable<Object>  callWrite = new ConnectionWrite(socket, message);
            result = threadPool.submit(callWrite);

            return result.get();

        } catch (Exception e) {
            System.out.println("Issue with the request");
            e.printStackTrace();
        }

        return null;


    }

    public void shutdown(){
        threadPool.shutdown();
    }


    private class ConnectionWrite implements Runnable, Callable<Object> {

        Socket serverConnection;
        private ObjectOutputStream outputStream;
        private Object objectToWrite;


        private ConnectionWrite(Socket serverSocket, Object obj) throws IOException{
            serverConnection = serverSocket;
            outputStream = new ObjectOutputStream(serverSocket.getOutputStream());
            objectToWrite = obj;
        }

        public Object call() {

            Object obj = null;

            try {
                outputStream.writeObject(objectToWrite);
                ObjectInputStream inputStream = new ObjectInputStream(serverConnection.getInputStream());
                obj = inputStream.readObject();

            } catch (Exception e){
                e.printStackTrace();
            }
            return obj;
        }


        @Override
        public void run() {


            try{
                outputStream.writeObject(objectToWrite);
                outputStream.flush();

                if (outputStream != null){
                    outputStream.close();
                }
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

}
