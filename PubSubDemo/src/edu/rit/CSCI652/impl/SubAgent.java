/**
 * Author:  Andrew Heyman
 * Course:  CSCI-652
 * Project: Project 1.1
 */

package edu.rit.CSCI652.impl;

import edu.rit.CSCI652.demo.*;

import java.io.ObjectInputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.List;
import java.util.Scanner;


public class SubAgent extends Agent implements Subscriber{

    private int listenPort;

    public SubAgent(String eventManagerIP, int eventManagerPort, int listenPort ){
        super(eventManagerIP, eventManagerPort);
        this.listenPort = listenPort;


        // Spawn a thread that is listening for incoming data
        Runnable inThread = () -> {

            try {
                ServerSocket ss = new ServerSocket(listenPort);
                while (true) {
                    Socket socket = ss.accept();
                    ObjectInputStream stream = new ObjectInputStream(socket.getInputStream());
                    Object obj = stream.readObject();

                    if (obj instanceof Event) {
                        System.out.println(obj);
                    } else if (obj instanceof List) {
                        for (Object element : (List)obj){
                            System.out.println(element);
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        };

        threadPool.submit(inThread);

    }

    @Override
    public void subscribe(Topic topic) {
        submitMessage(new SubscriberMessage(Operation.SUBSCRIBE, topic, listenPort));
    }


    @Override
    public void subscribe(String keyword) {
        // find all of the topics
        // subscribe to each

    }

    @Override
    public void unsubscribe(Topic topic) {
        submitMessage(new SubscriberMessage(Operation.UNSUBSCRIBE, topic, listenPort));
    }

    @Override
    public void unsubscribe() {
        submitMessage(new SubscriberMessage(Operation.UNSUBSCRIBE, listenPort));
    }

    @Override
    public List listSubscribedTopics() {

        Object obj = submitAndReceive(new SubscriberMessage(Operation.LIST_SUBSCRIBED_TOPICS, listenPort));

        if (obj instanceof  List){
            return (List)obj;
        } else{
            return null;
        }

    }

    public List listAllTopics() {

        Object obj = submitAndReceive((new SubscriberMessage(Operation.LIST_ALL_TOPICS, listenPort)));

        if (obj instanceof List){
            return (List)obj;
        } else{
            return null;
        }

    }

    public void cacheCheck() {
        Object result = submitAndReceive(new SubscriberMessage(Operation.CACHE_CHECK, listenPort));

        if (result instanceof List) {
            if (((List) result).size() > 0) {
                System.out.println("===========CACHED ITEMS==============");
                for (Object element : (List) result) {
                    System.out.println(element);
                }
                System.out.println("=====================================");
            }
        }


    }



    public static void main(String[] args){

        final String END = "quit";
        int eventManagerPort = -1;
        int listenPort = -1;
        String eventManagerIP = "";

        if (args != null && args.length == 3){
            eventManagerIP = args[0];
            eventManagerPort = Integer.parseInt(args[1]);
            listenPort = Integer.parseInt(args[2]);

        } else {
            System.out.println("Must have 3 arguments");
            System.out.println("arg 1: event manager IP, arg 2: event manager port, arg 3: port to listen for events");
            System.exit(1);
        }
        System.out.println("Connection to event manager on port: " + eventManagerPort);
        SubAgent agent = new SubAgent(eventManagerIP, eventManagerPort, listenPort);

        System.out.println();
        System.out.println();

        agent.cacheCheck();


        //=============
        // Simple UI
        //=============
        System.out.println("\'1\' to view topics");
        System.out.println("\'2\' to subscribe");
        System.out.println("\'3\' to view subscribed topics");
        System.out.println("\'4\' to unsubscribe");
        System.out.println("\'Quit\' to end the subscriber");
        System.out.print("Subscriber >");
        Scanner sc = new Scanner(System.in);
        String input = sc.nextLine();
        while (!input.toLowerCase().equals(END)){

            if (Integer.parseInt(input) == 1){
                for (Object topic : agent.listAllTopics()){
                    System.out.println(topic);
                }
            }

            if (Integer.parseInt(input) == 2){

                List result = agent.listAllTopics();
                Object[] array = result.toArray();
                System.out.println("Select a topic to subscribe to: ");

                for (int idx = 0; idx < array.length; idx++){
                    System.out.println("" + idx + ". " + array[idx]);
                }
                input = sc.nextLine();
                agent.subscribe((Topic)array[Integer.parseInt(input)]);
            }

            if (Integer.parseInt(input) == 3){
                for (Object topic : agent.listSubscribedTopics()){
                    System.out.println(topic);
                }
            }

            if (Integer.parseInt(input) == 4){
                List result = agent.listSubscribedTopics();
                Object[] array = result.toArray();

                for (int idx = 0; idx < array.length; idx++){
                    System.out.print("" + idx + ". " + array[idx]);
                }
                input = sc.nextLine();
                agent.unsubscribe((Topic)array[Integer.parseInt(input)]);
            }

            System.out.print("Subscriber > ");
            input = sc.nextLine();

        }

        agent.shutdown();
        System.exit(0);
    }

}
