/**
 * Author:  Andrew Heyman
 * Course:  CSCI-652
 * Project: Project 1.1
 */

package edu.rit.CSCI652.impl;


import edu.rit.CSCI652.demo.*;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

public class EventManager{

    private HashMap<Node, List<Event>> cachedEvents;
    private Hashtable<Topic, List<Node>>  topicMap;
    final private ExecutorService threadPool = Executors.newFixedThreadPool(10);


    public EventManager(){
        cachedEvents = new HashMap<>();
        topicMap = new Hashtable<>();
    }

    /**
     * Initialize the event manager
     * @param port port to listen on
     */
	public void startService(int port) {

        Runnable serverTask = () -> {
            try {
                ServerSocket serverSocket = new ServerSocket(port);

                while (true) {
                    Socket clientSocket = serverSocket.accept();
                    threadPool.submit(new ConnectionTask(clientSocket));
            }
            } catch (IOException e) {
                System.out.println("Issue with the request");
                e.printStackTrace();
            }
        };
        Thread serverThread = new Thread(serverTask);
        serverThread.start();
	}


    /**
     * Send Event to topic subscribers
     * @param event event to publish
     */
	private void notifySubscribers(Event event) {

	    List<Node> subscribers = topicMap.get(event.getTopic());
	    List<Node> onlineSubs =  Collections.emptyList();

	    if (subscribers != null){
            onlineSubs = subscribers.stream()
                    .filter(x -> x != null && x.isOnline)
                    .collect(Collectors.toList());
        }


	    for( Node agnt : onlineSubs){
	        Runnable serverTask = () -> {
	            try {

                    ObjectOutputStream stream = new ObjectOutputStream(
                            new Socket(agnt.address, agnt.port).getOutputStream());

	                stream.writeObject(event);

                } catch (IOException e){
	                System.err.println("Could not connect");
	                agnt.isOnline = false;
	                List<Event> clientCachedEvents = cachedEvents.get(agnt);
	                if (clientCachedEvents == null){
	                    try {
                            clientCachedEvents = new ArrayList<>();
                            clientCachedEvents.add(event);
                            cachedEvents.put(agnt,clientCachedEvents);
                        } catch (Exception a){
	                        a.printStackTrace();
                        }

                    } else{
                        clientCachedEvents.add(event);
                    }
                }
            };

            threadPool.submit(serverTask);

        }

        cacheEvents(event);
	}

    /**
     * Add event to the in-memory event store
     * @param event event to store
     */
	private void cacheEvents(Event event){

        List<Node> subscribers = topicMap.get(event.getTopic());
        List<Node> offlineSubs =  Collections.emptyList();
        if (subscribers != null){
            offlineSubs = subscribers.stream()
                    .filter(x -> x != null && !x.isOnline)
                    .collect(Collectors.toList());
        }

        for(Node offline: offlineSubs){
            List<Event> cached = cachedEvents.get(offline);
            if (cached == null){
                cached = new ArrayList<>();
                cached.add(event);
            }else{
                cached.add(event);
            }

        }
    }

    /**
     * Add topic to Map
     * @param topic new topic
     */
	private void addTopic(Topic topic){

        if (!topicMap.containsKey(topic)){
            topicMap.put(topic, new ArrayList<>());
        }
		
	}

    /**
     * Add a subscriber to the topic map
     * @param topic topic that exists
     * @param node subscriber
     */
	private void addSubscriber(Topic topic, Node node){
		List<Node> nodes = topicMap.get(topic);
        nodes.add(node);
	}

    /**
     *
     * @param topic topic to drop subscriber
     * @param node subscriber
     */
	private void removeSubscriber(Topic topic, Node node){
        List<Node> nodes = topicMap.get(topic);
        nodes.remove(node);
	}

    /**
     * Remove subscriber from every list
     * @param node subscriber to drop
     */
    private void removeSubscriber(Node node){

        for (Map.Entry<Topic, List<Node>> entry : topicMap.entrySet()){
            entry.getValue().remove(node);
        }

    }

    /**
     * Display subscribers for topic
     * @param
     */
	public void showSubscribers(){

	    for(Map.Entry<Topic,List<Node>> element : topicMap.entrySet()){
	        System.out.println("Topic: " + element.getKey());
            for(Node node : element.getValue()){
                System.out.println("\t" + node);
            }
        }
	}

	public List<Topic> topicsForSubscriber(Node node){

	    List<Topic> result = new ArrayList<>();

        for (Map.Entry<Topic, List<Node>> entry : topicMap.entrySet()){
            if (entry.getValue().contains(node)){
                result.add(entry.getKey());
            }
        }

        return result;

    }

    /**
     * Class to be run in its own thread when a new connection is made
     */
    private class ConnectionTask implements Runnable {
        private final Socket clientSocket;
        ObjectInputStream inputStream;


        private ConnectionTask(Socket clientSocket) throws IOException {
            this.clientSocket = clientSocket;
            inputStream = new ObjectInputStream(clientSocket.getInputStream());
        }

        @Override
        public void run() {
            // A connection has been made, perform the operation
            try {
                Object o = inputStream.readObject();
                InetAddress address = clientSocket.getInetAddress();

                if (!(o instanceof Message)) {
                    throw new Exception("Not a message");
                }

                if (o instanceof PublisherMessage) {
                    // If it's a publish message, we can close the connection and process
                    handlePublisher((PublisherMessage) o);

                } else if (o instanceof SubscriberMessage) {
                    handleSubscriber((SubscriberMessage) o, new Node(address, ((SubscriberMessage) o).getListeningPort(), NodeType.SUBSCRIBER));
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }


        /**
         * Handle Publish/Advertise
         * @param message received message
         */
        private void handlePublisher(PublisherMessage message) {

            try {
                switch (message.getOperation()) {

                    case PUBLISH:
                        clientSocket.close();
                        notifySubscribers(message.getEvent());
                        break;
                    case ADVERTISE:
                        clientSocket.close();
                        addTopic(message.getTopic());
                        break;

                    case LIST_ALL_TOPICS:
                        ObjectOutputStream os = new ObjectOutputStream(clientSocket.getOutputStream());
                        // Write the subscribe
                        List<Topic> topics = new ArrayList<>(topicMap.keySet());
                        os.writeObject(topics);
                        clientSocket.close();
                }
            } catch (Exception e){

            }

        }

        private void handleSubscriber(SubscriberMessage message, Node node) {

            node.isOnline = true; // Anytime we receive a message, ensure the node is labeled as on

            switch (message.getOperation()) {

                // On startup, provide the messages to the client
                case CACHE_CHECK:
                    List<Event> events =  cachedEvents.get(node);

                    if (events == null){
                        events = new ArrayList<>();
                    }

                    try {
                        ObjectOutputStream os = new ObjectOutputStream(clientSocket.getOutputStream());
                        os.writeObject(new ArrayList<Object>(events));
                    } catch (IOException e) {
                        System.err.println("Issue with " + node);
                        e.printStackTrace();
                    }

                    break;

                case SUBSCRIBE:
                    try {
                        clientSocket.close(); // No need to keep the application alive
                    } catch (Exception e) {
                        System.err.println("Issue with " + node);
                        e.printStackTrace();
                    }

                    addSubscriber(message.getTopic(), node);
                    break;

                case UNSUBSCRIBE:
                    try {
                        clientSocket.close();
                    } catch (Exception e) {
                        System.err.println("Issue with " + node);
                        e.printStackTrace();
                    }

                    if (message.getTopic() == null) {
                        removeSubscriber(node);
                    } else {
                        removeSubscriber(message.getTopic(), node);
                    }
                    break;

                case LIST_SUBSCRIBED_TOPICS:
                    try {
                        ObjectOutputStream os = new ObjectOutputStream(clientSocket.getOutputStream());
                        // Write the subscribe
                        os.writeObject(topicsForSubscriber(node));
                        clientSocket.close();
                    } catch (Exception e) {
                        System.err.println("Issue with " + node);
                        e.printStackTrace();
                    }
                case LIST_ALL_TOPICS:
                    try {
                        ObjectOutputStream os = new ObjectOutputStream(clientSocket.getOutputStream());
                        // Write the subscribe
                        List<Topic> topics = new ArrayList<>(topicMap.keySet());
                        os.writeObject(topics);
                        clientSocket.close();
                    } catch (Exception e) {
                        System.err.println("Issue with " + node);
                        e.printStackTrace();
                    }

                default:
                    break;
            }
        }
    }


    private class Node{
	    private InetAddress address;
	    private int port;
	    private NodeType type;
	    private boolean isOnline;

	    Node(final InetAddress address, final int port, final NodeType type){
	        this.address = address;
	        this.port = port;
	        this.type = type;
        }


        @Override
        public int hashCode() {
            return address.hashCode() + port + type.hashCode();
        }

        @Override
        public boolean equals(Object obj) {
            if (obj instanceof Node){
                Node that = (Node)obj;
                return that.address.equals(this.address) &&
                        that.port == this.port &&
                        that.type == this.type;
            }

            return false;
        }

        @Override
        public String toString() {
            return "Node: " + type + "\n"
                    + "Online: " + isOnline+ "\n"
                    + "IP Address: " + address+ "\n"
                    + "Port: " + port;
        }
    }

    private enum NodeType{
	    SUBSCRIBER, PUBLISHER
    }

    public static void main(String[] args){

        EventManager em = new EventManager();
        final String END = "quit";
        int port = 8000;
        if (args != null && args.length > 0){
            port = Integer.parseInt(args[0]);
        }
        System.out.println("Starting Event Manager on port: " + port);
        em.startService(port);

        //=============
        // Simple UI
        //=============
        System.out.println("\'1\' to view topic subscribers");
        System.out.println("\'Quit\' to end the event manager");
        System.out.println(">>");
        Scanner sc = new Scanner(System.in);
        String input = sc.nextLine();
        while (!input.toLowerCase().equals(END)){

            if (input.equals("1")){
                em.showSubscribers();
            }

            System.out.println(">>");
            input = sc.next();
        }

        em.threadPool.shutdown();
        System.exit(0);
    }

}
