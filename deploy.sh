# This should be performed initially
#git clone https://gitlab.com/aheyman/csci652-aheyman-proj-1_1.git
cd ..

for node in eventmanager subscriber1 subscriber2 subscriber3 publisher1 publisher2 publisher3
do
	# Start the container
	docker run -d --name=$node -it peiworld/csci652
	
	# Copy the source code
	docker cp csci652-aheyman-proj-1_1 $node:/
	
	# Find the source files
	docker exec -d $node  sh -c " find / -type f -wholename  "/csci652-aheyman-proj-1_1/*.java" > /csci652-aheyman-proj-1_1/source.txt"
	
	# Make a build directory 
	docker exec -d $node  mkdir /csci652-aheyman-proj-1_1/build
		
	# Build
	docker exec -d $node sh -c "cd /csci652-aheyman-proj-1_1/ && javac -d /csci652-aheyman-proj-1_1/build @source.txt"
		
done








